import Vue from "vue"
import VueRouter from "vue-router"

Vue.use(VueRouter)

import Index from "@/components/index"
import Recommendation from "@/components/recommendation"
import About from "@/components/about"
import Develop from "@/components/develop"
import NotFound from "@/components/notFound"

import BookIndex from "@/components/book/index"
import BookChapters from "@/components/book/chapters"
import BookContent from "@/components/book/content"

const routes = [
  {
    path: "/",
    name: "Index",
    component: Index
  },
  {
    path: "/recommendation",
    name: "Recommendation",
    component: Recommendation
  },
  {
    path: "/about",
    name: "About",
    component: About
  },
  {
    path: "/develop",
    name: "Develop",
    component: Develop
  },
  {
    path: "/book",
    name: "BookIndex",
    component: BookIndex
  },
  {
    path: "/book/:bookId",
    name: "BookChapters",
    component: BookChapters
  },
  {
    path: "/book/:bookId/chapter/:chapterId",
    name: "BookContent",
    component: BookContent
  },
  // 放到最后
  {
    path: "*",
    component: NotFound
  }
]

const router = new VueRouter({
  routes
})

// 跳转后返回顶部
router.afterEach(() => {
  window.scrollTo(0, 0);
})

export default router
