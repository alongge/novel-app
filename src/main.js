import Vue from 'vue'
import App from './app.vue'
import router from "./router/index"
import 'vue-beauty/package/style/vue-beauty.min.css'
import VueBeauty from 'vue-beauty'
import { getFunc, postFunc, putFunc, deleteFunc } from "./api/httpService"
import InfiniteLoading from 'vue-infinite-loading'

Vue.use(VueBeauty)
Vue.use(InfiniteLoading)
Vue.config.productionTip = false

Vue.prototype.$get = getFunc
Vue.prototype.$post = postFunc
Vue.prototype.$put = putFunc
Vue.prototype.$delete = deleteFunc

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
